"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      student.hasMany(models.tracking);
      student.belongsTo(models.service, { foreignKey: "serviceId" });
      student.belongsTo(models.child, { foreignKey: "childId" });
    }
  }
  student.init(
    {
      serviceId: DataTypes.INTEGER,
      childId: DataTypes.INTEGER,
      status : DataTypes.STRING
    },
    {
      sequelize,
      modelName: "student",
    }
  );
  return student;
};

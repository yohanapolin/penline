"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tracking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // tracking.belongsTo(models.service, { foreignKey: "serviceId" });
      tracking.belongsTo(models.student, { foreignKey: "studentId" });
    }
  }
  tracking.init(
    {
      createdAt: DataTypes.DATE,
      description: DataTypes.STRING,
      achievement: DataTypes.STRING,
      behavior: DataTypes.STRING,
      studentId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "tracking",
    }
  );
  return tracking;
};

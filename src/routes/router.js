const express = require("express");
const router = express.Router();

const controller = require("../controllers/authController");

const TutorDashboard = require("../controllers/studentController");
const ServiceController = require("../controllers/serviceController");
const ParentDashboard = require("../controllers/parentController");
const serviceController = require("../controllers/serviceController");

var checkAuthentication = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/login");
  }
};

//ROUTING
router.get("/", controller.getHomepage);
router.get("/login", controller.getLoginPage);
router.get("/register", controller.getregister);
router.get("/logout", checkAuthentication, controller.logout);
router.post("/register", controller.register);
router.post("/login", controller.login);

//ROUTING TUTOR
//ROUTING GET
router.get("/tutor-dashboard", checkAuthentication, TutorDashboard.dashboardTutor);
router.post("/tutor-dashboard", checkAuthentication, TutorDashboard.searchStudent);
router.get("/student/:id", checkAuthentication, TutorDashboard.studentDetail);
router.get("/tracking/:id", checkAuthentication, TutorDashboard.getDetailLaporan);
router.get("/profile-tutor", checkAuthentication, TutorDashboard.getDataTutor);
router.post("/profile-tutor", checkAuthentication, TutorDashboard.updateDataTutor);
router.get("/offer", checkAuthentication, TutorDashboard.listPenawaran);
router.get("/offer/:id", checkAuthentication, TutorDashboard.detailPenawaran);
router.post("/offer/confirm/:id", checkAuthentication, TutorDashboard.confirmPenawaran);
router.post("/offer/reject/:id", checkAuthentication, TutorDashboard.rejectPenawaran);
router.get("/add-tracking", checkAuthentication, TutorDashboard.getaddLaporan);
router.post("/add-tracking", checkAuthentication, TutorDashboard.createLaporan);

//ROUTING POST

//ROUTING DELETE

//ROUTING PUT

//ROUTING ORANG TUA
//ROUTING GET
router.get("/service", checkAuthentication, serviceController.listService);
// router.post("/service", checkAuthentication, serviceController.searchTutor);

router.post("/service", checkAuthentication, serviceController.filteringTutor);
router.get("/addservice", checkAuthentication, serviceController.getaddservice);
router.post("/addservice", checkAuthentication, serviceController.addservice);
router.get('/addsubject', checkAuthentication, serviceController.getsubject );
router.post("/addsubject", checkAuthentication, serviceController.addsubject);
router.get("/addmaterial", checkAuthentication, serviceController.getAddMaterial);
router.post("/addmaterial", checkAuthentication, serviceController.addMaterial);
router.get("/service/:id", checkAuthentication, serviceController.detailservice);
router.get("/parents-dashboard", checkAuthentication, ParentDashboard.getDashboardOrtu);
router.get("/tambahLaporan", checkAuthentication, controller.getTambahLaporan);
router.get("/detailLaporan", checkAuthentication, controller.getDetailLaporan);
router.get("/detailMurid", checkAuthentication, controller.getDetailMurid);
router.get("/addchild", checkAuthentication, ParentDashboard.getAddChild);
router.get("/child/:id", checkAuthentication, ParentDashboard.getDetail);
router.get("/child/edit/:id", checkAuthentication, ParentDashboard.geteditchild);
router.post("/child/edit", checkAuthentication, ParentDashboard.editChild);
router.get("/laporan/:id", checkAuthentication, ParentDashboard.detailLaporan);
router.get("/penawaran", checkAuthentication, ParentDashboard.listPenawaran);
router.get("/penawaran/:id", checkAuthentication, ParentDashboard.detailPenawaran);
router.get("/listservice", checkAuthentication, serviceController.listService);
//ROUTING POST
router.post("/addchild", checkAuthentication, ParentDashboard.create);
router.post("/pesan", checkAuthentication, ServiceController.pesanservice);
//ROUTING DELETE
//ROUTING PUT
//auth Routing

module.exports = router;

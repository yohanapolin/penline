// run this command to terminal for generate data seed
// Alangkah baiknya jika membuat db baru untuk melakukna test seeder
// Langkah sebagai berikut
// 1. Edit nama databse di bagian development pada file config/database.json
// 2. Jalankan perintah => sequelize db:create
// 3. Jalankan perintah => squelize db:migrate
// 4. Jalankan perintah => sequelize db:seed:all

"use strict";
const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // seed for table subjects
    await queryInterface.bulkInsert(
      "subjects",
      [
        {
          id: 1,
          name: "Biologi",
          kelas: "1 SMP",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          name: "Matematika",
          kelas: "1 SMP",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          name: "Fisika",
          kelas: "1 SMP",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );

    // seed for table materials
    await queryInterface.bulkInsert(
      "materials",
      [
        {
          id: 1,
          name: "Biologi",
          title: "Pengantar",
          description: "Pengantar",
          subjectId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          name: "Biologi",
          title: "Bab 1",
          description: "Pengantar",
          subjectId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          name: "Biologi",
          title: "Bab 2",
          description: "Pengantar",
          subjectId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          name: "Matematika",
          title: "Pengantar",
          description: "Pengantar",
          subjectId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 5,
          name: "Matematika",
          title: "Bab 1",
          description: "Pengantar",
          subjectId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 6,
          name: "Matematika",
          title: "Bab 2",
          description: "Pengantar",
          subjectId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );

    // seed for table users
    await queryInterface.bulkInsert(
      "users",
      [
        {
          id: 1,
          username: "orangtua",
          email: "ot@ot.com",
          password: bcrypt.hashSync("123", 10),
          role: "ORANGTUA",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          username: "tutor1",
          email: "t1@t1.com",
          password: bcrypt.hashSync("123", 10),
          role: "TUTOR",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          username: "tutor2",
          email: "t2@t2.com",
          password: bcrypt.hashSync("123", 10),
          role: "TUTOR",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );

    // seed for table children
    await queryInterface.bulkInsert(
      "children",
      [
        {
          id: 1,
          name: "Adi",
          kelas: "1 SMP",
          age: 12,
          birthday: new Date(),
          deskripsi: "Anaknya suka insecure",
          userId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          name: "Wawan",
          kelas: "1 SMP",
          age: 11,
          birthday: new Date(),
          deskripsi: "Yesaya 5:22",
          userId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          name: "Wati",
          kelas: "1 SMP",
          age: 11,
          birthday: new Date(),
          deskripsi: "Every day is a fresh new hell",
          userId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          name: "Bambang",
          kelas: "1 SMP",
          age: 11,
          birthday: new Date(),
          deskripsi: "Dia suka sama yang lain",
          userId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );

    // seed for table children
    await queryInterface.bulkInsert(
      "services",
      [
        {
          id: 1,
          subjectId: 1,
          userId: 2,
          name: "Adi",
          rating: 5,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          subjectId: 2,
          userId: 2,
          name: "Wati",
          rating: 5,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          subjectId: 2,
          userId: 2,
          name: "Wawan",
          rating: 5,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          subjectId: 2,
          userId: 2,
          name: "Bambang",
          rating: 5,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );

    // seed for table children
    await queryInterface.bulkInsert(
      "students",
      [
        {
          id: 1,
          serviceId: 1,
          childId: 1,
          status: "PENDING",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          serviceId: 2,
          childId: 3,
          status: "DITERIMA",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          serviceId: 3,
          childId: 2,
          status: "DITERIMA",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          serviceId: 4,
          childId: 4,
          status: "DITERIMA",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};

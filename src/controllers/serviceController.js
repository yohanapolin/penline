const { child, student, service, tracking, user, subject, material } = require("../models");

class serviceController {
  static listService = (req, res) => {
    if (req.user.role == "ORANGTUA") {
    service.findAll({ include: [{ model: user }, { model: subject, include: { model: material } }] }).then((service) => {
      subject.findAll({}).then((subject)=>{
        res.render("pages/hasilPencarian.ejs", { data: service, data1: subject })
      });
    });
  }
  else {
    res.render("login.ejs");
  }
  };
  static detailservice = (req, res) => {
    if (req.user.role == "ORANGTUA") {
    const id = req.params.id;
    service.findAll({ where: { id: id }, include: [{ model: user }, { model: subject, include: { model: material } }] }).then((service)=> {
      child.findAll({ where: { userId: req.user.id } }).then( (child)=> {
        res.render("pages/detailservice.ejs",{
          data: service,
          data1: child}
        );
      });
    });
  }
  else {
    res.render("login.ejs");
  }
  };
  static pesanservice = (req, res) => {
    if (req.user.role == "ORANGTUA") {
    const students = {
      serviceId: req.body.serviceId,
      childId: req.body.childId,
      status: "PENDING",
    };
    student.create(students).then(() => {
      res.redirect("/service");
    });
  }
  else {
    res.render("login.ejs");
  }
  };
  static getsubject = (req, res) => {
    if (req.user.role == "TUTOR") {
    res.render("pages/data-subject.ejs");
    }
    else {
      res.render("login.ejs");
    }
  }
  
  static addsubject = (req, res) => {
    if (req.user.role == "TUTOR") {
    const subjects = {
      name: req.body.name,
      kelas: req.body.kelas
    }
    subject.create(subjects).then(()=>{
      res.redirect("/addservice")
    } );
  }
    else {
      res.render("login.ejs");
    }
  };

  static addMaterial = (req, res) => {
    if (req.user.role == "TUTOR") {
    const materials = {
      name: req.body.name,
      title:req.body.title,
      description:req.body.description,
      subjectId:req.body.subjectId
    }
    material.create(materials).then(() => {
      res.redirect("/addservice");
    });
  }
  else {
    res.render("login.ejs");
  }
  }
  static getAddMaterial = (req, res) => {
    subject.findAll().then(subjects=> 
      res.render("pages/data-material.ejs", {data : subjects}))
  };
  static getaddservice = (req, res) => {
    subject.findAll().then(subject=> 
      res.render("pages/data-service.ejs", {data :subject} ))
  };
  

  static addservice = (req, res) => {
    const id = req.user.id
    const services = {
      subjectId : req.body.subjectId,
      userId: id,
      name : req.body.name
    }
    service.create(services).then(() => {
      res.redirect("/service")
    });
};


  // search berdasarkan nama tutor
  static searchTutor = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const { cari } = req.body;

      service
        .findAll({
          include: [
            { model: user, where: { username: { [require("sequelize").Op.like]: "%" + cari + "%" } } },
            { model: subject, include: { model: material } },
          ],
        })
        .then((service) => {
          subject.findAll({}).then((subject)=>{
            res.render("pages/hasilPencarian.ejs", { data: service, data1: subject })
          });
          // res.render("hasilPencarian.ejs", { data: service });
        })
        .catch((err) => {
          res.json({
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  // filtering service berdasarkan subject
  static filteringTutor = (req, res) => {
    let idSubject = req.body.subjectName;
    if (req.user.role == "ORANGTUA") {
      if (typeof idSubject !== "undefined") {
        service
          .findAll({
            include: [{ model: user }, { model: subject, where: { id: idSubject}, include: { model: material } }],
          })
          .then((service) => {
            subject.findAll({}).then((subject)=>{
              res.render("pages/hasilPencarian.ejs", { data: service, data1: subject })
            })
            .catch((err) => {
              res.json({
                message: err,
              });
            });
          })
          .catch((err) => {
            res.json({
              message: err,
            });
          });
      } else {
        service
          .findAll({
            include: [{ model: user }, { model: subject, include: { model: material } }],
          })
          .then((service) => {
            subject.findAll({}).then((subject)=>{
              res.render("pages/hasilPencarian.ejs", { data: service, data1: subject })
            })
            .catch((err) => {
              res.json({
                message: err,
              });
            });
          })
          .catch((err) => {
            res.json({
              message: err,
            });
          });
      }
    } else {
      res.render("login.ejs");
    }
  };
}

module.exports = serviceController;

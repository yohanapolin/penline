const { child, student, service, tracking, user } = require("../models");

class studentcont {
  static dashboardTutor = (req, res) => {
    if (req.user.role == "TUTOR") {
      const id = req.user.id;
      student
        .findAll({
          include: [
            {
              model: child,
            },
            {
              model: service,
              where: { userId: id },
            },
          ],
        })
        .then((student) => {
          res.render("dashboardTutor.ejs", {
            data: student,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static dashboardTutor = (req, res) => {
    if (req.user.role == "TUTOR") {
      student
        .findAll({
          where: { status: "DITERIMA" },
          include: [
            {
              model: child,
            },
            { model: service, where: { userId: req.user.id } },
          ],
        })
        .then((student) => {
          res.render("dashboardTutor.ejs", {
            data: student,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static studentDetail = async (req, res) => {
    if (req.user.role == "TUTOR") {
      const id = req.params.id;
      tracking
        .findAll({ include: { model: student, where: { childId: id }, include: [{ model: child }, { model: service }] } })
        .then((tracking) => {
          res.render("detailMurid.ejs", {
            data: tracking,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static getLaporan = async (req, res) => {
    if (req.user.role == "TUTOR") {
      const id = req.params.id;
      tracking
        .findAll({
          include: [
            {
              model: student,
              where: { id: id, status: "DITERIMA" },
              include: [
                {
                  model: child,
                },
              ],
            },
            { model: service },
          ],
        })
        .then((tracking) => {
          res.render("detailMurid.ejs", {
            data: tracking,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static createLaporan = (req, res) => {
    if (req.user.role == "TUTOR") {
      const laporan = {
        studentId: req.body.studentId,
        createdAt: req.body.createdAt,
        description: req.body.description,
        achievement: req.body.achievement,
        behavior: req.body.behavior,
      };
      tracking.create(laporan).then(() => {
        res.redirect("tutor-dashboard");
      });
    } else {
      res.render("login.ejs");
    }
  };

  static getaddLaporan = (req, res) => {
    if (req.user.role == "TUTOR") {
      student.findAll({ include: [{ model: child }, { model: service, where: { userId: req.user.id } }], where: { status: "DITERIMA" } }).then((student) => {
        res.render("tambahLaporan.ejs", { data: student });
      });
    } else {
      res.render("login.ejs");
    }
  };

  static getDetailLaporan = (req, res) => {
    if (req.user.role == "TUTOR") {
      const id = req.params.id;
      tracking
        .findAll({ where: { id: id }, include: { model: student, include: [{ model: child }, { model: service }] } })
        .then((tracking) => {
          res.render("detailLaporan.ejs", {
            data: tracking,
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static listPenawaran = (req, res) => {
    if (req.user.role == "TUTOR") {
      const id = req.user.id;
      student
        .findAll({
          include: [
            { model: service, where: { userId: id }},
            { model: child, include: { model: user }},
          ]
        })
        .then((service) => {
          res.render("tutor_penawaran.ejs", {
            data: service,
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static detailPenawaran = (req, res) => {
    if (req.user.role == "TUTOR") {
      const userId = req.user.id;
      const id = req.params.id;
      student
        .findAll({
          where: { id: id },
          include: [
            { model: service, where: { userId: userId } },
            { model: child, include: { model: user } },
          ],
        })
        .then((student) => {
          res.render("tutor_menunggu.ejs", { data: student });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static confirmPenawaran = (req, res) => {
    const id = req.params.id;
    if (req.user.role == "TUTOR") {
      student
        .update({ status: "DITERIMA" }, { where: { id: id } })
        .then(res.redirect("/offer"))
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static rejectPenawaran = (req, res) => {
    const id = req.params.id;
    if (req.user.role == "TUTOR") {
      student
        .update({ status: "DITOLAK" }, { where: { id: id } })
        .then(res.redirect("/offer"))
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  // profile tutor
  static getDataTutor = (req, res) => {
    user
      .findOne({ where: { id: req.user.id } })
      .then((response) => {
        console.log(response);
        res.render("profile", {
          data: response,
          message: "This is the data",
        });
      })
      .catch((e) => {
        res.json({
          status: 422,
          message: "Error get Tutor",
        });
      });
  };

  // update profile tutor
  static updateDataTutor = (req, res) => {
    const { fullname, deskripsi, jeniskelamin, address, phonenumber, gaji, provinsi } = req.body;
    user
      .update(
        { fullname, deskripsi, jeniskelamin, address, phonenumber, gaji, provinsi },
        {
          where: { id: req.user.id },
        }
      )
      .then(() => {
        user
          .findOne({ where: { id: req.user.id } })
          .then((response) => {
            console.log(response);
            res.render("profile", {
              data: response,
              message: "This is the data",
            });
          })
          .catch((e) => {
            res.json({
              status: 422,
              message: "Error get Tutor",
            });
          });
      })
      .catch((e) => {
        res.json({
          status: 422,
          message: e,
        });
      });
  };

  // search berdasarkan nama murid
  static searchStudent = (req, res) => {
    if (req.user.role == "TUTOR") {
      const { cari } = req.body;
      student
        .findAll({
          where: { status: "DITERIMA" },
          include: [
            {
              model: child,
              where: { name: { [require("sequelize").Op.like]: "%" + cari + "%" } },
            },
            {
              model: service,
              where: { userId: req.user.id },
            },
          ],
        })
        .then((student) => {
          res.render("dashboardTutor.ejs", {
            data: student,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static getListService = (req, res)=>{
    if (req.user.role == "TUTOR") {
    service.findAll({where : { userId : id}}).then((service) => 
    res.render("listservice.ejs", { data : service}))
    .catch((err) => {
      res.json({
        message : err
      });
    });
  }else {
    res.render("login.ejs")
  }
  }
}

module.exports = studentcont;

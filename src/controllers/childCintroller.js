const { user, child } = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");
const { render } = require("ejs");
const { use } = require("passport");

// Create and Save a new Tutorial
class Children {
  static getAll = (req, res) => {
    res.render("test_design/create_child");
  };

  static create = (req, res) => {
    if (!req.body.name) {
      res.status(400).send({
        message: "Content can not be empty!",
      });
      return;
    }
    const children = {
      name: req.body.name,
      age: req.body.age,
      kelas: req.body.kelas,
      birthday: req.body.birthday,
      deskripsi: req.body.deskripsi,
      userId: req.user.id,
    };
    console.log(children);

    child
      .create(children)
      .then((data) => {
        res.redirect("/parents-dashboard", data);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

  static getchild = (req, res) => {
    const childAll = [];
    order: [["userId", "DESC"]],
      child
        .findAll({
          include: [
            {
              model: user,
            },
          ],
          where: {
            userId: req.user.id,
          },
        })
        .then((child) => {
          for (const children of child) {
            childAll.unshift({
              name: children.dataValues.name,
              age: children.dataValues.age,
              kelas: children.dataValues.kelas,
              birthday: children.dataValues.birthday,
              deskripsi: children.dataValues.deskripsi,
              userId: children.dataValues.id,
            });
          }

          res.render("pages/dashboard-orang-tua.ejs", {
            data: childAll,
          });
        });
  };
  static getDetail = async (req, res) => {
    const id = req.params.id;
    tracking
      .findAll({
        include: [
          {
            model: student,
            include: [
              {
                model: child,
                where: { id: id },
              },
            ],
          },
          { model: service },
        ],
      })
      .then((tracking) => {
        res.render("pages/detail-child.ejs", {
          data: tracking,
          message: "This is the data",
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };

  // Find a single Tutorial with an id
  static findOne = (req, res) => {
    const id = req.params.id;

    child
      .findByPk(id)
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message: "Error retrieving Tutorial with id=" + id,
        });
      });
  };

  // Update a Tutorial by the id in the request
  static update = (req, res) => {
    const id = req.params.id;
    const children = {
      name: req.body.name,
      age: req.body.age,
      class: req.body.class,
      userId: req.user.id,
    };
    child
      .update(children, {
        where: { id: id },
        where: { userId: req.user.id },
      })
      .then((num) => {
        if (num == 1) {
          res.send({
            message: "Tutorial was updated successfully.",
            data: num,
          });
        } else {
          res.send({
            message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Error updating Tutorial with id=" + id,
        });
      });
  };
}
module.exports = Children;

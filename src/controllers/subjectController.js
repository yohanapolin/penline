const { subject } = require("../models");

class subjectcontroller {
  static index = (req, res) => {
    subject
      .findAll({})
      .then((response) => {
        res.json({
          data: response,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Server Error",
        });
      });
  };

  static create = (req, res) => {
    const { name, kelas } = req.body;
    subject
      .create({ name, kelas })
      .then((response) => {
        res.json({
          status: 200,
          message: "Pass",
          data: response,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Server Error",
        });
      });
  };

  static update = (req, res) => {
    const subjectId = req.params.id;
    const { name, kelas } = req.body;
    subject
      .update(
        {
          name,
          kelas,
        },
        {
          where: { id: subjectId },
        }
      )
      .then((response) => {
        res.json({
          status: 201,
          data: response,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "Can't Update Subject",
        });
      });
  };
  static delete = (req, res) => {
    const subjectId = req.params.id;
    subject
      .destroy({
        where: {
          id: subjectId,
        },
      })
      .then(() => {
        res.json({
          status: 200,
          message: "Subject Deleted",
        });
      });
  };
}

module.exports = subjectcontroller;

const { child, student, service, tracking, user } = require("../models");

class ParentsController {
  // controller get static web for post.
  static getAddChild = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      res.render("pages/data-anak.ejs");
    } else {
      res.render("login.ejs");
    }
  };
  static geteditchild = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.params.id
      child.findAll({where : {id : id}}).then(children =>
        
      res.render("pages/edit-anak.ejs", {data : children}));
    } else {
      res.render("login.ejs");
    }
  };

  static editChild = (req, res) => {
    const id = req.body.id
    if (req.user.role == "ORANGTUA") {
      const children = {
        name: req.body.name,
        age: req.body.age,
        kelas: req.body.kelas,
        birthday: req.body.birthday,
        deskripsi: req.body.deskripsi,
        userId: req.user.id
      }
      child.update(children, {where : { id : id}}).then(()=> 
      res.redirect("/parents-dashboard"))
      
    } else {
      res.render("login.ejs");
    }
  }
  static getDashboardOrtu = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const childAll = [];
      order: [["userId", "DESC"]],
        child
          .findAll({
            include: [
              {
                model: user,
              },
            ],
            where: {
              userId: req.user.id,
            },
          })
          .then((child) => {
            for (const children of child) {
              childAll.unshift({
                name: children.dataValues.name,
                age: children.dataValues.age,
                kelas: children.dataValues.kelas,
                birthday: children.dataValues.birthday,
                deskripsi: children.dataValues.deskripsi,
                userId: children.dataValues.id,
              });
            }
            res.render("pages/dashboard-orang-tua.ejs", {
              data: childAll,
            });
          });
    } else {
      res.render("login.ejs");
    }
  };

  static getDetail = async (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.params.id;
      tracking
        .findAll({ include: { model: student, where: { childId: id }, include: [{ model: child }, { model: service }] } })
        .then((tracking) => {
          res.render("pages/detail-child.ejs", {
            data: tracking,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };
  static create = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      if (!req.body.name) {
        res.status(400).send({
          message: "Content can not be empty!",
        });
        return;
      }
      const children = {
        name: req.body.name,
        age: req.body.age,
        kelas: req.body.kelas,
        birthday: req.body.birthday,
        deskripsi: req.body.deskripsi,
        userId: req.user.id,
      };
      console.log(children);

      child
        .create(children)
        .then((data) => {
          res.redirect("parents-dashboard");
        })
        .catch((err) => {
          res.status(500).send({
            message: err.message || "Some error occurred while creating the Tutorial.",
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static studentDetail = async (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.params.id;
      console.log("thisss id " + id);
      student
        .findAll(
          {
            include: [
              {
                model: child,
                include: [
                  {
                    model: user,
                  },
                ],
              },
              {
                model: service,
              },
            ],
          },
          {
            where: {
              id: id,
            },
          }
        )
        .then((student) => {
          res.render("detailMurid.ejs", {
            data: student,

            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };
  static detailLaporan = async (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.params.id;
      tracking
        .findAll({ where: { id: id }, include: { model: student, include: [{ model: child }, { model: service }] } })
        .then((tracking) => {
          res.render("pages/detail-laporan.ejs", {
            data: tracking,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };
  static listPenawaran = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.user.id;
      student
        .findAll({
          include: [
            { model: service, include: { model: user } },
            { model: child, include: { model: user }, where: { userId: id } },
          ],
        })
        .then((service) => {
          res.render("pages/penawaran-terakhir.ejs", {
            data: service,
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static getLaporan = async (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.params.id;
      tracking
        .findAll({
          include: [
            {
              model: student,
              include: [
                {
                  model: child,
                  where: { id: id },
                },
              ],
            },
            { model: service },
          ],
        })
        .then((tracking) => {
          res.render("detailMurid.ejs", {
            data: tracking,
            message: "This is the data",
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static createLaporan = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const laporan = {
        serviceId: req.body.serviceId,
        studentId: req.body.childId,
        createdAt: req.body.createdAt,
        description: req.body.description,
        achievement: req.body.achievement,
        behavior: req.body.behavior,
      };
      tracking.create(laporan).then((tracking) => {
        res.render("tambahLaporan.ejs", {
          data: tracking,
        });
      });
    } else {
      res.render("login.ejs");
    }
  };

  static getaddLaporan = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      res.render("tambahLaporan.ejs");
    } else {
      res.render("login.ejs");
    }
  };

  static getDetailLaporan = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const id = req.params.id;
      tracking
        .findAll({
          where: { id: id },
          include: { model: service },
        })
        .then((tracking) => {
          res.render("detailLaporan.ejs", {
            data: tracking,
          });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };
  // static listPenawaran = (req, res) => {
  // const id = req.user.id
  // student.findAll({include : [{ model : service,
  //                         where : {userId : id}},
  //                     { model : child ,
  //                     include : {model : user} }]})
  // .then((service)=> {
  //     res.render("tutor_penawaran.ejs", {
  //         data : service
  //     } )
  // })
  // .catch((err)=>{
  //     res.json({
  //         status : 500,
  //         message : err
  //     })
  // })
  // }

  static detailPenawaran = (req, res) => {
    if (req.user.role == "ORANGTUA") {
      const userId = req.user.id;
      const id = req.params.id;

      student
        .findAll({
          where: { id: id },
          include: [
            { model: service, include: { model: user } },
            { model: child, include: { model: user } },
          ],
        })
        .then((student) => {
          res.render("pages/detail-penawaran.ejs", { data: student });
        })
        .catch((err) => {
          res.json({
            status: 500,
            message: err,
          });
        });
    } else {
      res.render("login.ejs");
    }
  };

  static confirmPenawaran = (req, res) => {
    if (req.user.role == "ORANGTUA") {
    } else {
      res.render("login.ejs");
    }

    const userId = req.params.id;
    student
      .update({
        include: { model: service, status: "DITERIMA", where: { userId: userId } },
      })
      .then(res.redirect("/service"))
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };
}
module.exports = ParentsController;

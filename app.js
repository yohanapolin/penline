const express = require("express");
const path = require("path");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var session = require("express-session");
const bcrypt = require("bcrypt");
const moment = require("moment");


const router = require("./src/routes/router");
const { user } = require("./src/models");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set("views", path.join(__dirname, "/src/views"));
app.set("view engine", "ejs");
app.use(express.static("src/public"));
//Express session
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);

// passport config
passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    (email, password, done) => {
      console.log("masuk passport");
      user
        .findOne({
          where: {
            email: email,
          },
        })
        .then((user) => {
          console.log(user);
          if (user && user.dataValues) {
            const encryptedPassword = user.dataValues.password;

            if (bcrypt.compareSync(password, encryptedPassword)) {
              return done(null, user);
            } else {
              return done(null, false);
            }
          } else {
            return done(null, false);
          }
        });
    }
  )
);
passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((obj, done) => {
  done(null, obj);
});
passport.authenticate("local", {
  failureFlash: "Invalid username or password.",
});
passport.authenticate("local", {
  failureFlash: "Invalid username or password.",
});

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use("/", router);

app.use((err, req, res, next) => {
  if (err) {
    console.log(err);
  }
});
app.use((req, res, next)=>{
  res.locals.moment = moment;
  next();
});

const PORT = process.env.PORT || 3000

app.listen(PORT, () => console.log(`Example app listening at http://localhost:${PORT}`))
module.exports = app;

module.exports = app;
